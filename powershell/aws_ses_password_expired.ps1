# Define username and password
$smtpUserName = 'username'
$smtpPassword = ConvertTo-SecureString 'password' -AsPlainText -Force

#debug. Not send mail only terminal output
$debug = 0

#variant set new password or send invetion
$flag = 'sendpass' #sendinv

# Convert to SecureString
[pscredential]$credential = New-Object System.Management.Automation.PSCredential ($smtpUserName, $smtpPassword)

$ExpireDays = 1
Import-Module ActiveDirectory

$AllUsers = get-aduser -filter * -properties * |where {$_.Enabled -eq "True"} | where { $_.PasswordNeverExpires -eq $false } | where { $_.passwordexpired -eq $false } | where { $_.emailaddress -match "@eisgroup.com" }
#$AllUsers =  get-aduser -filter * -properties * | where { $_.PasswordNeverExpires -eq $false } | where { $_.emailaddress -like "dzvenyhorodskyi@eisgroup.com" }


function Get-RandomCharacters($length, $characters) {
    $random = 1..$length | ForEach-Object { Get-Random -Maximum $characters.length }
    $private:ofs=""
    return [String]$characters[$random]
}
 
function Scramble-String([string]$inputString){     
    $characterArray = $inputString.ToCharArray()   
    $scrambledStringArray = $characterArray | Get-Random -Count $characterArray.Length     
    $outputString = -join $scrambledStringArray
    return $outputString 
}



foreach ($User in $AllUsers){
  $PasswdSetDate = (get-aduser $User -properties * | foreach { $_.PasswordLastSet })
  $MaxPasswdAge = (Get-ADDefaultDomainPasswordPolicy).MaxPasswordAge
  $ExpireDate = $PasswdSetDate + $MaxPasswdAge
  $Today = (get-date)
  $DaysToExpire = (New-TimeSpan -Start $Today -End $ExpireDate).Days

  if($DaysToExpire -le $ExpireDays -and $DaysToExpire -gt 0 ){
      $Name = (Get-ADUser $User | foreach { $_.Name})
      $UserName = (Get-ADUser $User | foreach { $_.SamAccountName})
      $Email = $User.emailaddress
      
      if($flag -eq 'sendinv'){
          $EmailSubject="[AD] Password Expiry Notice - your password expires in $DaystoExpire days"
          $EmailBody="Hi $Name,`nYour AD domain password for account '$UserName'.`nPlease change the password as soon as possible to prevent further logon problems.`nDocumentation how change your password: wiki "
          echo "$flag $Email $UserName $DaysToExpire | $PasswdSetDate $MaxPasswdAge"      

          if($debug -eq 0){
            #set change password in logon
            Set-ADUser -Identity $UserName -ChangePasswordAtLogon $true

            Send-MailMessage  -Credential $credential `
	                          -useSSL `
	                          -smtpServer 'email-smtp.us-east-1.amazonaws.com' `
	                          -port 587 `
	                          -from 'from_mail' `
	                          -to $Email `
	                          -subject $EmailSubject `
                              -body $EmailBody
          }
      }

      if($flag -eq 'sendpass'){
      
          $password = Get-RandomCharacters -length 8 -characters 'abcdefghiklmnoprstuvwxyz'
          $password += Get-RandomCharacters -length 1 -characters 'ABCDEFGHKLMNOPRSTUVWXYZ'
          $password += Get-RandomCharacters -length 1 -characters '1234567890'
          $password += Get-RandomCharacters -length 2 -characters '!$%&/()=?}{#'
          $password = Scramble-String $password

          echo $password

          $EmailSubject="[AD] Password Expiry Notice - your password was changed!"
          $EmailBody="Hi $Name,`nYour AD domain password for account '$UserName' is changed.`n`nNew password: $password`n`nPlease use this password for login to AD.`nDocumentation how configure vpn: wiki "
          echo "$flag $Email $UserName $DaysToExpire | $PasswdSetDate $MaxPasswdAge"      

          if($debug -eq 0){
            #set new password
            Set-ADAccountPassword -Identity $user -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$password" -Force)

            Send-MailMessage  -Credential $credential `
	                          -useSSL `
	                          -smtpServer 'email-smtp.us-east-1.amazonaws.com' `
	                          -port 587 `
	                          -from 'from_mail' `
	                          -to $Email `
	                          -subject $EmailSubject `
                              -body $EmailBody
         }
      }
  }
}
