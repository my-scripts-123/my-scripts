$('#FindTovars').click(function(){

    function modifyDOM() {  

        var codes_ar = "";
        var articul_ar = "";
        var text_cat = new Array();

        console.log('Tab script:');

        $('.table tr td[width="50"]').each(
            function(){
                $(this).parent().find('td').eq(1).addClass($(this).text())
                codes_ar   = codes_ar+$(this).text()+','
                articul_ar = articul_ar+$(this).parent().find('td').eq(3).text()+','                

            }
        )
        
        $.post($query_url, {
            dataQ: codes_ar,
            articul_ar: articul_ar
            }, function(q)  {  

                var ar = q.split(';');
                
                for (var i = 0; i < ar.length-1; i++) {
                    console.log(ar[i]);   
                  
                    var ar2 = ar[i].split('*');
                    var ar3 = ar2[1].split(',');

                    var out = "<b style='color: #000; font-size:15px;' class='art_"+ar2[0]+"'>"
                    for (var y = 0; y < ar3.length; y++) {                  
                        out = out + "SG = "+ar3[y]+"<br />"
                    }
                    out = out + "</b>";
                    $('.'+ar2[0]).html(out)
                }
               
                // create data for 1c
                var i_num = 0;
                $(' table[width*="100%"]').each(
                    function(){
                        if($('.my_1s_'+i_num).length == 0){
                            $('table h1').eq(i_num).append('<div class="my_1s_'+i_num+'"></div>')
                        }
                        var tovars = $(this).find('tr td[width="50"]');                        
                        var tovar_str = "";
                        var valut = 1;

                        if($('select[name *= balance_select]').val() == 6978){
                            valut = 0;
                        }

                        for (var y = 0; y < tovars.length; y++) {
                            q = $(tovars).eq(y).parent().find('td').eq(1).find('b').html();
                            q_ar = q.split('<br>');

                            for(var z = 0; z < q_ar.length-1; z++){                                                                
                                var tovar_code  = q_ar[z].replace("SG = ","")
                                var tovar_price = $(tovars).eq(y).parent().find('td').eq(6).text().replace(/[^\d\.]/g, "");
                                if(valut == 0){tovar_price = tovar_price.slice(0, -1)}
                                var tovar_kolvo = $(tovars).eq(y).parent().find('td').eq(8).text()                            
                                tovar_str = tovar_str + tovar_code+" "+tovar_price+" "+tovar_kolvo+" 0 "+valut+'\n';
                                console.log(tovar_str);                                
                            }

                            

                        }
                        // button copy text
                        if(q_ar.length > 2){ //if more than 1 code per line
                            $('.my_1s_'+i_num).addClass('warn');
                            $('.my_1s_'+i_num).html('<textarea style="display: block;" class="text_'+i_num+'">'+tovar_str+'</textarea><input type="button" value="COPY" style="background: #ff002d;color: #fff;padding: 10px;position: relative;" onclick="$('+"'"+'#text_'+i_num+''+"'"+').select();document.execCommand('+ "'copy'" +');$(this).val('+"'COPY OK'"+').css('+"'background'"+','+"'#5600ff'"+');alert('+"'Warning!'"+')">')
                        }else{
                            $('.my_1s_'+i_num).html('<textarea style="display: block;" id="text_'+i_num+'">'+tovar_str+'</textarea><input type="button" value="COPY" style="background: yellowgreen;color: #fff;padding: 10px;position: relative;"  onclick="$('+"'"+'#text_'+i_num+''+"'"+').select();document.execCommand('+ "'copy'" +');$(this).val('+"'COPY OK'"+').css('+"'background'"+','+"'blue'"+')">')
                        }
                        
                       i_num++;
                    }
                )
                                             
        });

        
        return document.body.innerHTML;
    }
    
    chrome.tabs.executeScript({    
        code: '(' + modifyDOM + ')();' //argument here is a string but function.toString() returns function's code
    }, (results) => {
        //Here we have just the innerHTML and not DOM structure
        console.log('Popup script:')
        console.log(results[0]);
    });

})
