#!/bin/bash
function Help(){
	echo Usage: ./remont.sh [ flag ] [ username ]
	echo -e  -rc all'\t'  Recalculate ALL mail accounts
	echo -e -rc denis@uniq.ua '\t'  Recalculate only one mailbox
}

function Recalculate(){
	if [[ $1 == "all" ]]; then
  		echo "Recalculate ALL mail accounts"
		doveadm quota recalc -A
	elif [[ $1 != "" ]]; then
  		echo "Recalculate only one mailbox $1"
		doveadm quota recalc -u $1
	else
  		echo "Error. Exit"
	fi
}

case $1 in
	-rc) Recalculate $2;;
	*) Help ;;

esac