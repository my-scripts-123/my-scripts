<?php

function send_telegram_bot($text){
	// сюда нужно вписать токен нашего бота
		define('TELEGRAM_TOKEN', '');

	// сюда нужно вписать наш внутренний айдишник
		define('TELEGRAM_CHATID', '');

    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL => 'https://api.telegram.org/bot' . TELEGRAM_TOKEN . '/sendMessage',
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_POSTFIELDS => array(
                'chat_id' => TELEGRAM_CHATID,
                'text' => $text,
            ),
        )
    );
    curl_exec($ch);
}


?>