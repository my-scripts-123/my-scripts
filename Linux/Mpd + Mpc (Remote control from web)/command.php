<?php

include('function.php');

function getConfig(){

	$handle = @fopen("/MusicPlayer/command_play.txt", "r");		
	if ($handle){
	    while (($line = fgets($handle)) !== false) {			       
	       if(@!strstr($line, "#") && strlen($line) > 10){
	       		$set = explode("|", $line);
	       }
	    }

	 	fclose($handle);
	}

	if(@count($set) == 0){
		$set = '120|play|http://87.229.53.9:8006|KekDunaHot'; // default config
		$set	= explode("|", $set);
	}

	return $set;

}

function writeConfig($set){	
	$new_settings = "";
	foreach ($set as $key => $line) {
		$new_settings .= $line;
		if($key < count($set)-1){
			$new_settings .= "|";
		}
	}
		
	$file = '/MusicPlayer/command_play.txt';
	$f = @fopen($file, "w");
		$current = "";
		$current .= "# 1st element - volume\n";
		$current .= "# 2st element - play|stop\n";
		$current .= "# 3st element - Radio stream\n";
		$current .= "# 4st element - radio name\n";		
		$current .= $new_settings;						
	file_put_contents($file, $current, FILE_APPEND);		
	@fclose($f);
	send_telegram_bot("write config \n\n".$new_settings);
}

function getStatusMPC(){
	$mpc_status = explode("\n", shell_exec("mpc -f %file% status"));
	return $mpc_status;	
}

function getTimeMPC($mpc_status, $t){
	$time = $t;
	if(strstr($mpc_status[1], '[playing]')){
		$time 	= explode("/", $mpc_status[1]); 
		$time = str_replace(":", "", substr($time[1],3));
		echo $time;
	}	
	return $time;
}

function getAlsa(){
	$alsa_status = [];
	$alsa_get_status 	= explode("\n", shell_exec("amixer -c1"));
	$i = -1;
	foreach ($alsa_get_status as $key => $valueAlsa) {
		if(strstr($valueAlsa, 'Simple')){
			$i++;
			$str0 = explode("'", $valueAlsa);
			$alsa_status[$i]['Output'] = $str0[1];
		}
		if(strstr($valueAlsa, 'Front Left:')){
			$str5 = explode(" ", $valueAlsa);
			$alsa_status[$i]['Volume'] = $str5[5];
			$alsa_status[$i]['VolumePercent'] = preg_replace("/[^0-9%]/", '', $str5[6]);		
		}
	}
	return $alsa_status;
}

function getVolume(){
	$alsa = getAlsa();
	return $alsa[0]['Volume'];

}


function getSheduler(){
	/*
		* 		- все дни
		1-7 	- нужные дни недели
		spec 	- специальные исключения (не рабочии дни магазина) date(Y-m-d)
	*/
	$ar_sheduler['*'] 	 = array(
								'start_time' => '09:45:00', 
								'stop_time'  => '20:10:00'
						   );
	$ar_sheduler[7] 	 = array(
								'start_time' => '09:45:00', 
								'stop_time'  => '19:10:00'
						   );	
	
	
	/* фильтр первого января*/
	if(date("m-d") == '01-01'){
		$ar_sheduler['*'] 	 = array('start_time' => '00:00:00', 'stop_time'  => '00:00:00');
		$ar_sheduler[7] 	 = $ar_sheduler['*'];
	}

	/* дни пасхи не играем */
	if(
		date('Y-m-d') == '2020-04-19' ||
		date('Y-m-d') == '2021-05-02' ||
		date('Y-m-d') == '2022-04-24' ||
		date('Y-m-d') == '2023-04-16' ||
		date('Y-m-d') == '2024-05-05' ||
		date('Y-m-d') == '2025-04-20'
	){
		$ar_sheduler['*'] 	 = array('start_time' => '00:00:00', 'stop_time'  => '00:00:00');
		$ar_sheduler[7] 	 = $ar_sheduler['*'];
	}
	if(
		date('Y-m-d') == '2020-04-26' ||
		date('Y-m-d') == '2021-05-09' ||
		date('Y-m-d') == '2022-05-01' ||
		date('Y-m-d') == '2023-04-23' ||
		date('Y-m-d') == '2024-05-12' ||
		date('Y-m-d') == '2025-04-27' ||
		date('Y-m-d') == '2020-02-044'
	){
		$ar_sheduler['*'] 	 = array('start_time' => '11:45:00', 'stop_time'  => '20:10:00');
		$ar_sheduler[7] 	 = array('start_time' => '11:45:00', 'stop_time'  => '19:10:00');
	}
	return $ar_sheduler;

}

function replace_double_dotes($time){
	$time = str_replace(":", "", $time);
	return $time;
}

function CanIPlayMusic($Sheduler){
	$this_day_week 	= date('N');
	$this_time 		= replace_double_dotes(date('H:i:s'));
	$current_key 	= 0; // ключ дня для массива
	$permission 	= 0; // 1 = разрешаем игарть, 0 = запрещаем

	if(@$Sheduler[$this_day_week]){
		$current_key = $this_day_week;		
		echo "\nIts good day $this_time\n";
	}else{
		$current_key = '*';		
	}
	$start_time 	= replace_double_dotes($Sheduler[$current_key]['start_time']);
	$stop_time 		= replace_double_dotes($Sheduler[$current_key]['stop_time']);
	
	if(
		$this_time >= $start_time &&
		$this_time <= $stop_time 
	){
		$permission = 1;
	}

	
	// принудительно перезаписываем комманду плей
	if($this_time > '090000' && $this_time < '091000'){	
		$set = getConfig();
		if($set[1] != 'play'){
			$set[1] = 'play';	
			writeConfig($set);
		}
		
		
	}

	return $permission;
}


function runB(){
	shell_exec("php /MusicPlayer/send_status_to_site.php");
	$set = getConfig();
	$current_volume = getVolume();
	$mpc_status = getStatusMPC();
	print_r($mpc_status);

	$getSheduler = getSheduler();
	$run_date_sheduller = CanIPlayMusic($getSheduler);

	/* сравниваем даты, и запускаем плеер */
	if($run_date_sheduller == 1){	

		/* проверяем громкость  из файла настроеек */
		if($current_volume != $set[0]){
			echo "\nVolume control\n";
			echo $current_volume." :: ".$set[0];
			shell_exec("amixer -c1 set Speaker ".$set[0]);
			send_telegram_bot('Изменили громкость с '.$current_volume.' на '.$set[0].' ['.date('H:i:s d.m.Y').']');				

		}

		/* переключаем станцию */
		if($mpc_status[0] != $set[2]){
			echo "\nrun new station\n";
			shell_exec("mpc clear");		// очищаем очередь
			shell_exec("mpc add ".$set[2].""); // добавляем очередь
			shell_exec("mpc play");		// запускаем очередь	
			send_telegram_bot('Новая станция '.$set[3].' ['.date('H:i:s d.m.Y').']');				
		}

		$mpc_status = getStatusMPC();
		$mpc_status_time_old = getTimeMPC(getStatusMPC(), 5);
		sleep(2); // ждем чтобы сравнить результат
		$mpc_status_time_new = getTimeMPC(getStatusMPC(), 16);
		$mpc_status_time_raznica = ($mpc_status_time_new-$mpc_status_time_old)/60;
		if($mpc_status_time_raznica < 1){$mpc_status_time_raznica = $mpc_status_time_raznica *60;}
		
		/* проверяем нет ли зависаний. И запущен ли плеер */
		if(($mpc_status_time_raznica == 0 || $mpc_status_time_raznica == 11) && $set[1] == 'play'){
			echo "\nrun zero $mpc_status_time_raznica\n\n";
			shell_exec("amixer -c1 set Speaker ".$set[0]);
			shell_exec("mpc clear");		// очищаем очередь
			shell_exec("mpc add ".$set[2].""); // добавляем очередь
			shell_exec("mpc play");		// запускаем очередь
			send_telegram_bot('Старт музыка ['.date('H:i:s d.m.Y').']');						
		}

	}


	/* оставнавливаем плеер */
	if($run_date_sheduller == 0 || $set[1] == 'stop'){
		echo "\nstop music\n";
		shell_exec("mpc stop");
		
		if(!strstr($mpc_status[0], 'volume: n/a')){
			send_telegram_bot('Стоп музыка ['.date('H:i:s d.m.Y').']');
			echo "\n****stop*****\n";
		}
		
	}

}

runB();

?>