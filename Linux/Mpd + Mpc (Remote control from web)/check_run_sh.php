<?php
/*
	Чекаем запущен ли скрипт который обменивается информацией 
	с сайтом и следит за работоспособностью плеера
*/
include('function.php');

$get_ps = explode("\n", shell_exec("ps aux | grep MusicPlayer/run.sh"));

$find_run_proccess = 0;
foreach ($get_ps as $key => $value) {	
	if(strstr($value, '/MusicPlayer/run.sh')){
		$find_run_proccess = 1;
	}
}
if($find_run_proccess == 0){
	send_telegram_bot('Запускаем музыкальный сервер ['.date('H:i:s d.m.Y').']');
	echo "run player\n\n";	
	exec('/MusicPlayer/run.sh');

	$file = '/MusicPlayer/test.txt';
	$f = @fopen($file, "w");
		$current = "";
		foreach ($get_ps as $key => $value) {
			$current .= "$value \n";
		}				
	file_put_contents($file, $current, FILE_APPEND);		
	@fclose($f);
}

echo "\n\n";
echo $find_run_proccess;

?>