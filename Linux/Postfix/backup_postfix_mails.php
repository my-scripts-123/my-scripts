<?php
	
$server_remote 		= check_galaxy_shluz();
$server_user 		= "";
$server_passwd 		= "";
$mail_spool 		= "/var/vmail/vmail1";
$sql_backup 		= "/var/vmail/backup/mysql";
$local_folder_backup 	= "/home/BackUp";
	
// Delete_old_copy
	echo "Delete Old Backup Files (".get_h_date().")\n\n";
	shell_exec('rm -rf /home/BackUp/*');

// GzipMailFolder
	echo "Copy and zipped mails (".get_h_date().")\n\n";
	$mail_file_name = $local_folder_backup."/mail_".get_date().".tar.gz";
	shell_exec("tar -cPzvf ".$mail_file_name." ".$mail_spool);

// GzipMysql
	echo "Copy and zipped sql backups (".get_h_date().")\n\n";
	$sql_file_name  = $local_folder_backup."/sql_".get_date().".tar.gz";
	shell_exec("tar -cPzvf ".$sql_file_name." ".$sql_backup);
	
// Upload to Backup Server	
    echo "Upload mail backups (".get_h_date().")\n\n";
 	shell_exec("curl -T ".$mail_file_name." ftp://".$server_remote."/MailBackup/mail_backup.tar.gz --user ".$server_user.":".$server_passwd);
	echo "Upload SQL backups (".get_h_date().")\n\n";
    shell_exec("curl -T ".$sql_file_name." ftp://".$server_remote."/MailBackup/sql_backup.tar.gz --user ".$server_user.":".$server_passwd);


function get_h_date(){
	$date = explode("_", get_date());
	$date[0] = str_replace("-", ".", $date[0]);	
	$date[1] = str_replace("-", ":", $date[1]);
	$date = $date[0]." ".$date[1];	

	return $date;
}

function get_date(){
	return date('d-m-Y_H-i-s');
}

function check_galaxy_shluz(){
	$ar_galaxy_ip[] = '1st_ip'; // Tenet	
	$ar_galaxy_ip[] = '2nd_ip'; // Vega
	$ar_galaxy_ip[] = '3th_ip'; // Soborka	
	$work_ip = "";

	foreach ($ar_galaxy_ip as $key => $value) {
	    $fp = @fsockopen ($value, 21, $errno, $errstr, 2);
	    if (@$fp) {
	        $work_ip = $value;
        	$work_ip_index = $key;
	        break;
	        @fclose ($fp);
	    }
	    @fclose ($fp);
	}

	return $work_ip;		
}

?>