<?php

function p24_xml($url_api, $data, $id_merchant, $password_hash){	
	$sign=sha1(md5($data.$password_hash));
	$xml = '<?xml version="1.0" encoding="UTF-8"?>
            <request version="1.0">
                <merchant>
                    <id>'.$id_merchant.'</id>
                    <signature>'.$sign.'</signature>
                </merchant>
                <data>
                	'.$data.'
                </data>
            </request>';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url_api);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	$response = curl_exec($ch);
	curl_close($ch);

	return simplexml_load_string($response);
}


function check_privatbank_card($card_number, $password_hash, $id_merchant $old_pb_array){	
	$arP24  = [];
	$today = date("d.m.Y"); $todayPB = date("Y-m-d");
	$yesterday = date('d.m.Y',strtotime($today . "-3 days"));
	
	$data = '<oper>cmt</oper>
             <wait>0</wait>
             <test>0</test>
             <payment id="">
                <prop name="sd" value="'.$yesterday.'" />
                <prop name="ed" value="'.$today.'" />
                <prop name="card" value="'.$card_number.'" />
             </payment>';

    $xmlRead = p24_xml('https://api.privatbank.ua/p24api/rest_fiz', $data, $id_merchant, $password_hash);

	$data = '<oper>cmt</oper>
            <wait>0</wait>
            <test>0</test>
            <payment id="">
            	<prop name="cardnum" value="'.$card_number.'" />
            	<prop name="country" value="UA" />
            </payment>';
	
	$xmlReadBalance = p24_xml('https://api.privatbank.ua/p24api/balance', $data, $id_merchant, $password_hash);
	
	if(@$xmlReadBalance){				
		$totalDay = 0;
		if(@$xmlRead->data->info->statements->statement){
			foreach ($xmlRead->data->info->statements->statement as $value) {
				if(!strstr((string) $value->attributes()->description, "«Копилку»")){
					$arP24[(string) $value->attributes()->trandate][(string) $value->attributes()->appcode]['card'] = (string) $value->attributes()->card;
					$arP24[(string) $value->attributes()->trandate][(string) $value->attributes()->appcode]['appcode'] = (string) $value->attributes()->appcode;
					$arP24[(string) $value->attributes()->trandate][(string) $value->attributes()->appcode]['trandate'] = (string) $value->attributes()->trandate;
					$arP24[(string) $value->attributes()->trandate][(string) $value->attributes()->appcode]['trantime'] = (string) $value->attributes()->trantime;
					$arP24[(string) $value->attributes()->trandate][(string) $value->attributes()->appcode]['amount'] = (string) $value->attributes()->amount;
					$arP24[(string) $value->attributes()->trandate][(string) $value->attributes()->appcode]['cardamount'] = (string) $value->attributes()->cardamount;
					$arP24[(string) $value->attributes()->trandate][(string) $value->attributes()->appcode]['rest'] = (string) $value->attributes()->rest;
					$arP24[(string) $value->attributes()->trandate][(string) $value->attributes()->appcode]['terminal'] = (string) $value->attributes()->terminal;
					$arP24[(string) $value->attributes()->trandate]['TotalDay'] = str_replace("UAH", "грн", (string) $value->attributes()->cardamount)*1 + @$arP24[(string) $value->attributes()->trandate]['TotalDay'];
				}				
			}
								
			/* if receive money send a message notification to jabber managers */
			if(count(@$arP24[$todayPB]) > count(@$old_pb_array[$todayPB])){
				foreach ($arP24[$todayPB]as $key => $value) {
					if(!@$old_pb_array[$todayPB][$key]){		
						if(str_replace("UAH", "", $value['cardamount']) *1 > 0){			
							sendToJabber("", $value['trantime']." || Зашли деньги ".$value['cardamount'].". Переводили ".$value['amount'].".","ext");
						}
					}
				}
			}			
			$query = query("UPDATE `config` SET  `value` = '".serialize($arP24)."' WHERE `name` = 'pb_check'") or die(mysql_error());			
		}
	}
	
	return $response;
}
?>