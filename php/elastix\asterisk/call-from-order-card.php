<?php
function phoneManager($phoneManager = ""){		
	if($_SESSION['id'] == 2000){$phoneManager = '109';}
	if($_SESSION['id'] == 3471){$phoneManager = '111';}
	if($_SESSION['id'] == 3535){$phoneManager = '120';}
	return $phoneManager;
}

function callFromZakazCardFunc($myHost, $phone, $phoneManager, $idOrder){
	
	/*	
		$myHost 		- elastix host
		$phone  		- phone client
		$phoneManager 	- manager phone number
		$idOrder 		- order number
	*/
	
	$strUser 	 = "web_call";	 		 // elastix user
	$strSecret 	 = "my_secret_password"; // password user elastix
	$strContext  = "web";				 // context
	$strWaitTime = "10"; $strPriority = "1"; $strMaxRetry = "2";
	$strChannel = "SIP/".$phoneManager;
	$strExten = "9".$phone; 
	$strCallerId = "Zakaz_".$idOrder."<Shop-Galaxy>"; // the name that will be displayed on screen phone

	/* check host availability */
	$fp = @fsockopen ($myHost, 5038, $errno, $errstr, 2); 
	if (!$fp) {echo "Host or port not availability =( "; die();}
	@fclose($fp);	

	if (is_numeric($strExten)){
		usleep(1000000);
		$oSocket = fsockopen($myHost, 5038, $errnum, $errdesc) or die("Connection to host failed");
		fputs($oSocket, "Action: login\r\n");
		fputs($oSocket, "Events: off\r\n");
		fputs($oSocket, "Username: $strUser\r\n");
		fputs($oSocket, "Secret: $strSecret\r\n\r\n");
		fputs($oSocket, "Action: originate\r\n");
		fputs($oSocket, "Channel: $strChannel\r\n");
		fputs($oSocket, "WaitTime: $strWaitTime\r\n");
		fputs($oSocket, "CallerId: $strCallerId\r\n");
		fputs($oSocket, "Exten: $strExten\r\n");
		fputs($oSocket, "Context: $strContext\r\n");
		fputs($oSocket, "Priority: $strPriority\r\n\r\n");
		fputs($oSocket, "Action: Logoff\r\n\r\n");
		fclose($oSocket);
	}

}
?>