<?php
function phoneManager($phoneManager = ""){      
    if($_SESSION['id'] == 2000){$phoneManager = '109';}
    if($_SESSION['id'] == 3471){$phoneManager = '111';}
    if($_SESSION['id'] == 3535){$phoneManager = '120';}
    return $phoneManager;
}

function viewPhoneInCall($myHost){
	/* 
        $myHost         - elastix host
        $phone          - phone client
        $phoneManager   - manager phone number
        $idOrder        - order number
    */

    /* check host availability */
    $fp = @fsockopen ($myHost, 5038, $errno, $errstr, 2); 
    if (!$fp) {echo "Host or port not availability =( "; die();}
    @fclose($fp);   
    
    $strUser     = "web_call";           // elastix user
    $strSecret   = "my_secret_password"; // password user elastix
    
    /* Get all active chennels */
    $socket = fsockopen($strHost, 5038, $errnum, $errdesc) or die("Connection to host failed");    
    fputs($socket, "Action: Login\r\n");
    fputs($socket, "UserName: $strUser\r\n");
    fputs($socket, "Secret: $strSecret\r\n\r\n");    
    fputs($socket, "Action: Command\r\n");
    fputs($socket, "Command: core show channels verbose\r\n\r\n");
    fputs($socket, "Action: Logoff\r\n\r\n");

	$arrNoAdd['Channel              Location             State   Application'] = 1;$arrNoAdd['Response'] = 1;$arrNoAdd['Message'] = 1;$arrNoAdd['Event'] = 1;$arrNoAdd['Privilege'] = 1;$arrNoAdd['Status'] = 1;

    $arr = [];$q = 0;
    while (!feof($socket)) {
        $token  = fgets($socket, 8192);
           if($token!=false && !@$arrNoAdd[$token] && strlen($token) > 4){
                 if(!strstr($token, "calls processed") && !strstr($token, "END COMMAND") && !strstr($token, "Asterisk Call Manager") && strstr($token, "Local")  && (strstr($token, "ext-queues") || strstr($token, "from-internal-xfer"))){
                    $str = explode(" ", preg_replace("/  +/"," ",$token));
                    $extPhone = "";
                   
                    if($str[1] == 'from-internal-xfer'){
                        if(strlen($str[7]) > 3){

                            // get manager number
                                $intPhone =  $str[2];
                                $arr[$intPhone][$q]['IntPhone'] = $intPhone;
                                $arr[$intPhone][$q]['Text'] = 'Input Call';

                            // get external number
                                $extPhone = $str[7];
                                $arr[$intPhone][$q]['ExtPhone'] = returnTruePhone($extPhone);
                        }
                    }else{
                          // get manager number
                            $intPhone =  substr($str[9], 6, 3);
                            $arr[$intPhone][$q]['IntPhone'] = $intPhone;
                            $arr[$intPhone][$q]['Text'] = 'Input Call';

                          // get external number
                            $extPhone = $str[7];
                            $arr[$intPhone][$q]['ExtPhone'] = returnTruePhone($extPhone);
                    }
                
                    $q++;                  
                }

                if(strstr($token, "Outgoing Line")){
                    $str = explode(" ", preg_replace("/  +/"," ",$token));                    

                    // get manager number
                        $intPhone =  substr($str[9], 4, 3);
                        $arr[$intPhone][$q]['IntPhone'] = $intPhone;
                        $arr[$intPhone][$q]['Text'] = 'Outgoing Call';

                    // get external number
                        $extPhone = $str[7];
                        $arr[$intPhone][$q]['ExtPhone'] = returnTruePhone($extPhone);                   
                        $q++;  
                }
            }   
    }

    fclose($socket);
    $phoneManager = phoneManager();

    return $arr[$phoneManager];
}
?>