<?  
	$mobUSBTypeU['U2'] = "USB20";
	$mobUSBTypeU['U3'] = "USB30";
	$Orders = [];
  $OrdersNoDelivStat = [];
  $OrdersSort = [];

function putXML($url, $idOrder, $status, $arNormalizeOstatok, $TTN = '', $username, $password){
  /* update TTN on Rozetka marketplace */
  $xml = '<Order>
            <records>
                <record>
                      <id>'.$idOrder.'</id>
                        <fields>
                            <status>'.$status.'</status>
                            <comment>Change Status From API</comment>
                            <ttn>'.$TTN.'</ttn>
                      </fields>
                </record>
            </records>
          </Order>';

  $curl = curl_init();    
  curl_setopt($curl, CURLOPT_URL, $url);      
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
  curl_setopt($curl, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
  curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);    
  curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
  $response = curl_exec($curl);    
  curl_close($curl);
    
  return $response;
}

function getXML($url, $xml = "", $username, $password){
  $curl = curl_init();    
  curl_setopt($curl, CURLOPT_URL, $url); 
  curl_setopt($curl, CURLOPT_USERPWD, $username.':'.$password);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
  curl_setopt($curl, CURLOPT_HEADER, 0);        
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
  $response = curl_exec($curl);
  curl_close($curl);  

  return $response;
}

for ($iRozetka = 0; $iRozetka < 3; $iRozetka++) { // берем 3 страницы с заказами = 150 заказов
	$getXML = simplexml_load_string(getXML("https://seller.rozetka.com.ua/api/order?group=process&part=".$iRozetka,$username, $password));
	
  foreach($getXML->records->record as $child){               
	  $Order['OrderID']                     = (string) $child->id;
	  $Order['OrderCreated']                = explode(" ", (string) $child->created);
	  $Order['OrderDate']                   = $Order['OrderCreated'][0];
	  $Order['OrderTime']                   = $Order['OrderCreated'][1];
	  $Order['OrderStatus']                 = (string) $child->status->id;
	  $Order['OrderComment']                = str_replace("'", "", (string) $child->comment);
	  $Order['OrderUserID']                 = (string) $child->user->id;
	  $Order['OrderUserFIO']                = str_replace("'", "", (string) $child->delivery->recipient);
	  $Order['OrderUserPhone']              = substr((string) $child->user->phone, 2, 10);
	  $Order['OrderUserMail']               = (string) $child->user->email;
	  $Order['OrderDeliveryService']        = (string) $child->delivery->service;
	  $Order['OrderDeliveryServiceID']      = (string) $child->delivery->serviceId;
	  $Order['OrderDeliverySkladUID']       = (string) $child->delivery->npWarehouseRef;
	  $Order['OrderDeliverySkladRecipient'] = str_replace("'", "", (string) $child->delivery->recipient);
	  $Order['OrderDeliveryCityRUS']        = (string) $child->delivery->city;
	  $Order['OrderDeliverySkladNumber']    = (string) $child->delivery->number;
    $Order['OrderDeliveryTTM']            = (string) $child->ttn;

	  $Order['OrderCart'] = [];
	  $i = 0;
	  
    foreach($child->purchase->record as $purchase){               
	    $Order['OrderCart'][$i]['Id'] = (string) $purchase->id;
	    $Order['OrderCart'][$i]['PriceEd'] = (string) $purchase->price;
	    $Order['OrderCart'][$i]['Name'] = (string) $purchase->title;
	    $Order['OrderCart'][$i]['KolVo'] = (string) $purchase->quantity;
	    $Order['OrderCart'][$i]['FabricID'] = (string) $purchase->offerId;
	    $i++;
	  }    

	  $Orders[$Order['OrderID']] = $Order;
	}
}



/*
  We use 2 stores on this marketplace. And for the correct import of orders you need to sort the 
  orders for our stores.
*/
foreach ($Orders as $key => $value) {
    $OrderCart = @$value['OrderCart'];unset($value['OrderCart']);

    if($OrderCart){
      foreach ($OrderCart as $key2 => $value2) {
        
          if(strlen($value2['FabricID']) == 10 || strlen($value2['FabricID']) == 11 || strlen($value2['FabricID']) == 9){
          	if(substr($value2['FabricID'], 0, 3) == "128"){
          		  $value2['TovarCode'] = substr($value2['FabricID'], 4, 5)*1;
              	$value2['GB'] = substr($value2['FabricID'], 0, 3)*1;
                if(strlen($value2['FabricID']) == 11){
                  $value2['TypeUSB'] = $mobUSBTypeU[substr($value2['FabricID'], 9, 2)];
                }else{
                  $value2['TypeUSB'] = $mobUSBTypeU[substr($value2['FabricID'], 8, 2)];  
                }
              	
          	}else{
          		$value2['TovarCode'] = substr($value2['FabricID'], 3, 5)*1;
              	$value2['GB'] = substr($value2['FabricID'], 0, 2)*1;
              	$value2['TypeUSB'] = $mobUSBTypeU[substr($value2['FabricID'], 8, 2)];	
          	}                        
              
              if(!@$OrdersSort['Uniq'][$key]){
                $OrdersSort['Uniq'][$key] = $value;
                $OrdersSort['Uniq'][$key]['TotalCost'] = 0;
              }           
              $OrdersSort['Uniq'][$key]['TotalCost'] = $OrdersSort['Uniq'][$key]['TotalCost']+$value2['PriceEd']*1*$value2['KolVo'];
              $OrdersSort['Uniq'][$key]['OrderCart'][] = $value2;

             if($value['OrderStatus'] != 1 && $value['OrderStatus'] != 3 && $value['OrderStatus'] != 5 && $value['OrderStatus'] != 4){
                 $OrdersNoDelivStat['Uniq'][$key] = $value['OrderStatus'];
             }
          }else{
              $value2T = explode("-",$value2['FabricID']);
              $value2['TovarCode'] = $value2T[1];
              if(!@$OrdersSort['SG'][$key]){
                $OrdersSort['SG'][$key] = $value;
                $OrdersSort['SG'][$key]['TotalCost'] = 0;
              }                   
              $OrdersSort['SG'][$key]['OrderCart'][] = $value2;
              $OrdersSort['SG'][$key]['TotalCost'] = $OrdersSort['SG'][$key]['TotalCost']+$value2['PriceEd']*1*$value2['KolVo'];

              if($value['OrderStatus'] != 1 && $value['OrderStatus'] != 3 && $value['OrderStatus'] != 5 && $value['OrderStatus'] != 4){
                 $OrdersNoDelivStat['SG'][$key] = $value['OrderStatus'];
              }    
          }
      }
    }
}

/* import orders to Uniq */
if(@$OrdersSort['Uniq']){
  foreach ($OrdersSort['Uniq'] as $key => $value) {
    if(1==1){
      $ref = "http://www.rozetka.ua";    
      $TotalCost = $value['TotalCost'];
      $kountTotalTovar = 0; $y = 0;
      $arNormalizeOstatok = []; $ar = [];
      
      foreach ($value['OrderCart'] as $key2 => $value2) {
        $kountTotalTovar = $kountTotalTovar + $value2['KolVo'];
        $arNormalizeOstatok[] = $value2['TovarCode'];
        $getPropTovarUniq = getPropTovarUniq($value2['TovarCode']);   
        
        $ar[$y] = array(                  
              'ID' => $value2['Id'],
              'TovarCode' => $value2['TovarCode'],
              'TovarLink' => "http://uniq.ua/mans-flash-usb/item=".$value2['TovarCode'],
              'KolVo' => $value2['KolVo'],
              'TovarPrice' => $value2['PriceEd']*1,
              'Total' => $value2['PriceEd']*1*$value2['KolVo'],
              'MarkerToarAdd' => 'flash',
              'GB' => $value2['GB'],
              'ValutInCart' => 'грн',
              'TovarName' => $getPropTovarUniq['Name'],
              'TovarImg' =>  "/images/optimized-images/".$value2['TovarCode']."/index_small.jpg",
              'OldPrice' => $value2['PriceEd']*1,
              'Akcion' => "",
              'TovarGroup' => $getPropTovarUniq['Category'],
              'FlashType' => $value2['TypeUSB'],                  
          );  

          $ar[$y]['Komplekt']['box'] = array(
                  'ID' => $value2['Id'],
                  'TovarCode' => $getPropTovarUniq['Box']['BoxCode'],
                  'TovarLink' => $value2['TovarCode'],
                  'KolVo' => $value2['KolVo'],
                  'TovarPrice' => 0,
                  'Total' => 0,
                  'MarkerToarAdd' => 'box',
                  'GB' => $value2['GB'],
                  'ValutInCart' => 'грн',
                  'TovarName' => 'Блистер',
                  'TovarImg' => $getPropTovarUniq['Box']['BoxPath'],
                  'OldPrice' => 0,
                  'Akcion' => "",
                  'TovarGroup' => "Accessories",
                  'FlashType' => $value2['TypeUSB'],
                );  
          $y++;
        }                

        if($value['OrderStatus'] == 1){
          $row_itemRoz = getOrderFromCart($key)
          if(!@$row_itemRoz['id']){
              insertToCart(serialize($ar));
              sendToJabber('', $time_now_time." || Uniq.ua: Новый заказ №".$last_id." на сумму ".$TotalCost." грн. ". iconv('Windows-1251', 'UTF-8', $fio).". [Rozetka.ua]",'');
          }
        }

        /* check TTN is update */
        $GetZakazTTN = getTTNFromOrder($key);
        if(@$GetZakazTTN['id']){
          if($GetZakazTTN['TTN'] != $value['OrderDeliveryTTM']){                  
            putXML("https://seller.rozetka.com.ua/api/Order", $key, 3, 10, $row_itemTTN['TTN'], $username, $password);
          }
        }
        
      }
  }
}

?>