resource "aws_instance" "hillel_srv" {
	ami		        = "${var.ami_id}"
	instance_type	= "${var.instance_type}"
	count		      = "${var.instance_count}"
	tags		      = "${var.tags}"
}

resource "aws_s3_bucket" "hillel_bucket" {
  bucket 	    	= "hillel-bucket"
  acl    	     	= "private"  

 	tags = {
 		Name        = "Hillel Bucket"
 	 	Environment = "Dev"
  	}

}