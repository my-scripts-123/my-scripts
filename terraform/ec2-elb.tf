resource "aws_instance" "hillel_srv" {
	ami				= "${var.ami_id}"
	instance_type	= "${var.instance_type}"
	count			= "${var.instance_count}"
	tags			= "${var.tags}"

}

resource "aws_elb" "elb" {
	name 						= "ec2elb"
	availability_zones 			= ["${aws_instance.srv.*.availability_zone}"]

	listener {
		instance_port			= 80
		instance_protocol		= "http"
		lb_port					= 80
		lb_protocol				= "http"

	}
	
	listener {
		instance_port			= 443
		instance_protocol		= "tcp"
		lb_port					= 443
		lb_protocol				= "tcp"

	}

	health_check {
		healthy_threshold		= 2
		unhealthy_threshold		= 2
		timeout					= 3
		target					= "TCP:22"
		interval				= 10
	}

	instances					= ["${aws_instance.srv.*.id}"]
	idle_timeout				= 300  # deffault value = 60
	connection_draining			= true
	connection_draining_timeout = "${var.tags}"
}